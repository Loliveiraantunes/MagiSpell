package alpha.dev.magispell.Domains;


import org.bukkit.Location;

public class Spawn {

    private  String name;

    private Location location;


    public Spawn(String name, Location location) {
        this.name = name;
        this.location = location;
    }

    public Spawn(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
