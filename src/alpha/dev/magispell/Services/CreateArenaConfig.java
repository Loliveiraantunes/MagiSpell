package alpha.dev.magispell.Services;

import alpha.dev.magispell.Domains.Arena;
import alpha.dev.magispell.Domains.Spawn;
import alpha.dev.magispell.Domains.Spectate;
import alpha.dev.magispell.Util.Config;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;


public class CreateArenaConfig {

    public Arena arena = new Arena();

    public Config config = Config.getInstance();

    public  Spawn spawn ;

    public  Spectate spectate ;

    FileConfiguration configuration = config.getConfig();

    public  CreateArenaConfig(){

    }

    public void CreateArena(String name){
        this.arena.setName(name);
    }

    public  void  AddSpawn(String name, Location location){
        spawn = new Spawn();
        List<Spawn> spawns = this.arena.getSpawn();
        spawn.setName(name);
        spawn.setLocation(location);
        spawns.add(spawn);
        this.arena.setSpawn(spawns);
        ClearObjects(this.spawn);
    }

    public boolean RemoveSpawn(String arena, String spawn){
        if(configuration.getString("Arena."+arena+".spawns."+spawn) != null){
            configuration.set("Arena."+arena+".spawns."+spawn,null);
            return  true;
        }
        return false;
    }

    public void AddSpectate(String name, Location location){
        spectate = new Spectate();
        List<Spectate> spectates = this.arena.getSpectate();
        spectate.setName(name);
        spectate.setLocation(location);
        spectates.add(spectate);
        this.arena.setSpectate(spectates);
        ClearObjects(this.spectate);
    }

    public  boolean  RemoveSpectate(String arena, String spectate){
        if(configuration.getString("Arena."+arena+".spectate."+spectate) != null) {
            configuration.set("Arena." + arena + ".spectate." + spectate, null);
            return true;
        }
        return false;
    }

    public  void setKillsToWin(int kills){
        this.arena.setKillsTowin(kills);
    }

    public  void SetMagicKit(boolean bool){
        this.arena.setMagic(bool);
    }

    public void  SaveArena(){
        configuration.set("Arena."+this.arena.getName(),"");
        for (Spawn spawn : this.arena.getSpawn()) {
            configuration.set("Arena."+this.arena.getName()+".spawns."+spawn.getName(),spawn.getLocation());
        }
        for (Spectate spectate:this.arena.getSpectate()) {
            configuration.set("Arena."+this.arena.getName()+".spectate."+spectate.getName(),spectate.getLocation());
        }
        configuration.set("Arena."+this.arena.getName()+".magic",this.arena.isMagic());
        configuration.set("Arena."+this.arena.getName()+".kills",this.arena.getKillsTowin());
        configuration.set("Arena."+this.arena.getName()+".InGame",this.arena.isInGame());
        config.SaveFile();
        ClearObjects(this.arena);

    }


    public Arena getArena(){
        return this.arena;
    }

    public void ClearObjects(Object obj){
        obj = null;
    }

}
