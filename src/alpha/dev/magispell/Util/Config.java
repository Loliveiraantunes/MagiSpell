package alpha.dev.magispell.Util;

import alpha.dev.magispell.Main;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class Config {

    private File file,config;
    private FileConfiguration fileConfiguration;

    static  Config configuration = new Config();


    private Config(){
        CreateConfig();
    }


    // Criar arquivo de configuração da arena...
    public void CreateConfig(){

        file = Main.getPlugin(Main.class).getDataFolder();
        config = new File(file, "Arena.yml");

        fileConfiguration = new YamlConfiguration();
        if(!file.exists()){
            try {
                file.mkdir();
            }catch (Exception ex){
                Main.getPlugin(Main.class).getServer().broadcastMessage("Não Foi possivel criar o arquivo YML: "+ex.getMessage());
            }
        }
        if(!config.exists()){
            try {
                config.createNewFile();
            }catch (Exception ex){
                Main.getPlugin(Main.class).getServer().broadcastMessage("Não Foi possivel criar o arquivo YML: "+ex.getMessage());
            }
        }
        loadConfig();
    }

    // Carregar o arquivo de configuração da arena.....
    public void loadConfig(){
        try {
            fileConfiguration.load(config);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    //Retorna a Config.yml
    public FileConfiguration getConfig(){
        return fileConfiguration;
    }

    // Salvar configuração......
    public void SaveFile(){
        try {
            fileConfiguration.save(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Config getInstance(){
        return configuration;
    }


}
