package alpha.dev.magispell.Util;

import alpha.dev.magispell.ItemArena.ItemArena;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CreateArenaCommand implements CommandExecutor {

    private static CreateArenaCommand arenaCommand = CreateArenaCommand.getInstance();

    private Commands commands = Commands.getInstance();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String cmdLabel, String[] cmdArgs) {

        if(commandSender instanceof Player && Permissions.HasPermission((Player) commandSender,"op")) {
            if (cmdLabel.equalsIgnoreCase("arena")) {

                if (cmdArgs[0].equalsIgnoreCase("create")) {
                   commands.CreateArena(cmdArgs);
                   commandSender.sendMessage(ChatColor.DARK_PURPLE+"@"+ChatColor.GREEN+" Adicione pelo menos "+ChatColor.RED+"2"+ChatColor.GREEN+" spawns e §c2 §a spectates na arena! use §c/§barena spawn add §a[nome]  e  §c/§barena spectate add §a[nome] ");
                }
                if(cmdArgs[0].equalsIgnoreCase("list")){
                   commandSender.sendMessage("§5@ §c"+commands.ListAll());
                }
                if(cmdArgs[0].equalsIgnoreCase("spawn")){

                    if(cmdArgs[1].equalsIgnoreCase("add")){
                        commands.AddSpawn(cmdArgs[2],((Player) commandSender).getLocation());
                        commandSender.sendMessage("§5@ §aSpawn §c"+cmdArgs[2]+"§a Adicionado com sucesso!");
                    }

                    if(cmdArgs[1].equalsIgnoreCase("del")){
                        commands.DelSpawn(cmdArgs[2],cmdArgs[3], (Player) commandSender);
                    }

                    if(cmdArgs[1].equalsIgnoreCase("list")){
                        commandSender.sendMessage("§5@ §a"+commands.ListSpawn(cmdArgs[2]));
                    }
                }
                if(cmdArgs[0].equalsIgnoreCase("spectate")){
                    if(cmdArgs[1].equalsIgnoreCase("add")){
                        commands.AddSpectate(cmdArgs[2],((Player) commandSender).getLocation());
                        commandSender.sendMessage("§5@ §aSpectate §c"+cmdArgs[2]+" §aAdicionado com sucesso!");
                    }
                    if(cmdArgs[1].equalsIgnoreCase("del")){
                        commands.DelSpectate(cmdArgs[2],cmdArgs[3], (Player) commandSender);
                    }
                    if(cmdArgs[1].equalsIgnoreCase("list")){
                        commandSender.sendMessage("§5@ §a"+commands.ListSpectate(cmdArgs[2]));
                    }
                }
                if(cmdArgs[0].equalsIgnoreCase("kill")){
                    commands.KillsToWin(Integer.parseInt(cmdArgs[1]));
                    commandSender.sendMessage("§5@ §aNumero de §c "+Integer.parseInt(cmdArgs[1])+" §a Mortes para Ganhar adicionado com sucesso!");
                }
                if(cmdArgs[0].equalsIgnoreCase("Magic")){
                    boolean magic;
                    if(cmdArgs[1].equalsIgnoreCase("true")){
                        magic = true;
                    }else{
                        magic = false;
                    }
                    commands.setMagic(magic);
                    commandSender.sendMessage("§5@ §aKit magico setado para §c"+magic);
                }

                if(cmdArgs[0].equalsIgnoreCase("save")){
                    commands.save((Player) commandSender);
                }
            }
            if(cmdLabel.equalsIgnoreCase("adm")){
                ((Player) commandSender).getInventory().clear();
                ((Player) commandSender).getInventory().setItem(0,ItemArena.getArenaName());
                ((Player) commandSender).getInventory().setItem(8,ItemArena.getCreateArena());
            }

            if(cmdLabel.equalsIgnoreCase("rename")){
            ItemStack item =((Player) commandSender).getItemInHand();
                ItemMeta itemMeta = item.getItemMeta();
                itemMeta.setDisplayName(cmdArgs[0]);
                item.setItemMeta(itemMeta);
                ((Player) commandSender).setItemInHand(item);
            }

        }
        return false;
    }

    public static CreateArenaCommand getInstance(){
        if(arenaCommand == null){
            arenaCommand = new CreateArenaCommand();
        }
        return  arenaCommand;
    }
}
