package alpha.dev.magispell.Domains;

import alpha.dev.magispell.Util.Config;

import java.util.ArrayList;
import java.util.List;

public class Arena {

    private String name;

    private List<Spectate> spectate = new ArrayList<>();

    private List<Spawn> spawn = new ArrayList<>();

    private  boolean InGame;

    private int killsTowin = 5;

    private boolean Magic;

    private Config config = Config.getInstance();

    public Arena(String name) {
        this.name = name;
    }
    public Arena(String name, List<Spectate> spectate, List<Spawn> spawn, boolean inGame, int killsTowin, boolean magic) {
        this.name = name;
        this.config = config;
        this.spectate = spectate;
        this.spawn = spawn;
        InGame = inGame;
        this.killsTowin = killsTowin;
        Magic = magic;
    }

    public List<Spectate> getSpectate() {
        return spectate;
    }

    public void setSpectate(List<Spectate> spectate) {
        this.spectate = spectate;
    }

    public List<Spawn> getSpawn() {
        return spawn;
    }

    public void setSpawn(List<Spawn> spawn) {
        this.spawn = spawn;
    }

    public Arena() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isInGame() {
        return InGame;
    }

    public void setInGame(boolean inGame) {
        InGame = inGame;
    }

    public int getKillsTowin() {
        return killsTowin;
    }

    public void setKillsTowin(int killsTowin) {
        this.killsTowin = killsTowin;
    }

    public boolean isMagic() {
        return Magic;
    }

    public void setMagic(boolean magic) {
        Magic = magic;
    }
}
