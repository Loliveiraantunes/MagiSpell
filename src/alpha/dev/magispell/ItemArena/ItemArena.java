package alpha.dev.magispell.ItemArena;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.List;

public class ItemArena {




    public static ItemStack getArenaName(){
        ItemStack ArenaName = new ItemStack(Material.PAPER);
        ItemMeta ArenaNameMeta = ArenaName.getItemMeta();
        ArenaNameMeta.setDisplayName("§aArena Name");
        List<String> lore = new ArrayList<>();
        lore.add("§a Use /Rename para Renomear");
        ArenaNameMeta.setLore(lore);
        ArenaName.setItemMeta(ArenaNameMeta);
        return ArenaName;
    }


    public  static  ItemStack getCreateArena(){
        ItemStack CreateArena = new ItemStack(Material.STAINED_GLASS, 1, (short) 5);
        ItemMeta CreateArenaMeta = CreateArena.getItemMeta();
        CreateArenaMeta.setDisplayName("§aCreate Arena");
        CreateArena.setItemMeta(CreateArenaMeta);
        return  CreateArena;
    }








}
