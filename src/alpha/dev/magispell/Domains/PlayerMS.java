package alpha.dev.magispell.Domains;

import org.bukkit.entity.Player;

public class PlayerMS {


    private Player player;

    private int kills;

    private  int deaths;

    private  int live;

    private boolean isDead;

    private boolean InGame;

    //dano causado;
    private int danoCausado;

    //dano recebido;

    private  int danoRecebido;


    public PlayerMS(Player player, int kills, int deaths, int live, boolean isDead, boolean inGame, int danoCausado, int danoRecebido) {
        this.player = player;
        this.kills = kills;
        this.deaths = deaths;
        this.live = live;
        this.isDead = isDead;
        InGame = inGame;
        this.danoCausado = danoCausado;
        this.danoRecebido = danoRecebido;
    }

    public PlayerMS() {
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public int getLive() {
        return live;
    }

    public void setLive(int live) {
        this.live = live;
    }

    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    public boolean isInGame() {
        return InGame;
    }

    public void setInGame(boolean inGame) {
        InGame = inGame;
    }

    public int getDanoCausado() {
        return danoCausado;
    }

    public void setDanoCausado(int danoCausado) {
        this.danoCausado = danoCausado;
    }

    public int getDanoRecebido() {
        return danoRecebido;
    }

    public void setDanoRecebido(int danoRecebido) {
        this.danoRecebido = danoRecebido;
    }
}
