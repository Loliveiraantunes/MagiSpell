package alpha.dev.magispell.Util;

import alpha.dev.magispell.Main;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class Cooldown {



    Main plugin = Main.getPlugin(Main.class);

    public void cooldown(Player player, int duration, ItemStack item) {

        new BukkitRunnable(){

            int count = duration;


            @Override
            public void run() {
                count --;
                try{
                    for (ItemStack itemStack : player.getInventory()) {
                        if(itemStack.isSimilar(item) && count > 0 ){
                            itemStack.setAmount(count);
                            if(itemStack.isSimilar(item) && count < 2 ){
                                player.sendMessage( item.getItemMeta().getDisplayName() + ChatColor.GREEN+" Está pronto para uso!");
                            }
                        }
                    }
                }catch (Exception ex){

                }
                if(count < 1) {
                    this.cancel();
                }
            }
        }.runTaskTimerAsynchronously(plugin,0,1*20);
    }



}
