package alpha.dev.magispell;

import alpha.dev.magispell.Util.Commands;
import alpha.dev.magispell.Util.Config;
import alpha.dev.magispell.Util.RegisterEvents;
import org.bukkit.ChatColor;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{


    public Plugin plugin = this;
    public Config config ;
    public PluginManager pm = this.getServer().getPluginManager();

    //Quando iniciar o Servidor
    @Override
    public void onLoad() {
        this.getServer().broadcastMessage(
                "" + ChatColor.GREEN + " --------------- Loading.... ------------\n");
        this.getServer().broadcastMessage(
                "" + ChatColor.GOLD +  "\n ----------------------------------------");
        this.getServer().broadcastMessage(ChatColor.AQUA+" - MagiSpell "+ ChatColor.GREEN +"V2.0   -by  "+ChatColor.GOLD+"AlphaDev_");
        this.getServer().broadcastMessage(
                "" + ChatColor.GOLD +  " ----------------------------------------\n");

        config.getInstance();
    }

    // Rodando durante o Jogo..
    @Override
    public void onEnable() {
        pm.registerEvents( new RegisterEvents(), this );
        Commands.getCommands();
    }

    // Quando finalizar eventos e o Server.
    @Override
    public void onDisable() {
        HandlerList.unregisterAll();

    }

}
