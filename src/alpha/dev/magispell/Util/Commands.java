package alpha.dev.magispell.Util;

import alpha.dev.magispell.Domains.Arena;
import alpha.dev.magispell.Main;
import alpha.dev.magispell.Services.CreateArenaConfig;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;

public class Commands {

    private  static Commands commands ;

    public  Config config = Config.getInstance();



    public CreateArenaConfig arenaConfig = new CreateArenaConfig();

    public static void getCommands(){
        Main.getPlugin(Main.class).getCommand("arena").setExecutor(CreateArenaCommand.getInstance());
        Main.getPlugin(Main.class).getCommand("adm").setExecutor(CreateArenaCommand.getInstance());
        Main.getPlugin(Main.class).getCommand("rename").setExecutor(CreateArenaCommand.getInstance());
    }

    public void CreateArena(String[] strings){
        arenaConfig.CreateArena(strings[1]);
     }

     public String ListAll(){
        return String.valueOf(config.getConfig().getConfigurationSection("Arena").getKeys(false));
     }

     public void AddSpawn(String name, Location location){
          arenaConfig.AddSpawn(name, location);
     }

    public void DelSpawn(String arena , String spawn , Player player ){

        if(arenaConfig.RemoveSpawn(arena, spawn)){
            player.sendMessage("§5@ §a"+spawn+" foi removido com sucesso!");
        } else
            player.sendMessage("§5@ §c"+spawn+" não existe !");
    }

    public String ListSpawn(String name){
        return String.valueOf(config.getConfig().getConfigurationSection("Arena."+name+".spawns").getKeys(false));
    }

    public void AddSpectate(String name, Location location){
        arenaConfig.AddSpectate(name, location);
    }

    public void DelSpectate(String arena , String spectate , Player player){
       if( arenaConfig.RemoveSpectate(arena, spectate)){
           player.sendMessage("§5@ §a"+spectate+" foi removido com sucesso!");
       } else
           player.sendMessage("§5@ §c"+spectate+" não existe !");
    }

    public String ListSpectate(String name){
        return String.valueOf(config.getConfig().getConfigurationSection("Arena."+name+".spectate").getKeys(false));
    }

    public  void save(Player player){
        if( arenaConfig.getArena().getSpawn().size() > 1){
            arenaConfig.SaveArena();
       player.sendMessage("§5@ §aArena criada com sucesso!");
        }else {
            player.sendMessage("§5@ §cArena necessita de pelo menos 2 spawns!");
        }
    }

    public void setMagic(boolean magic){
        arenaConfig.getArena().setMagic(magic);
    }



    public void KillsToWin(int kills){
        arenaConfig.getArena().setKillsTowin(kills);
    }


     public  static Commands getInstance(){
        if(commands == null){
            commands = new Commands();
        }
        return commands;
    }


}
